package arrayList;

/**
 * Created by Marek on 05.09.2017.
 */
public class MyArrayList<V> {
    private Integer initialSize=2000000;
    private Integer currentSize=0;
    private V[] array =(V[])new Object[initialSize];

    public void add(V data){

        array[currentSize]=data;
        currentSize++;
    }

    public void add(V data, Integer index){
        currentSize++;
        for (int i = 0; i < currentSize-index; i++) {
            array[currentSize-i]=array[currentSize-i-1];
        }
        array[index]=data;
    }

    public void printList(){
        System.out.println();
        for (int i = 0; i < currentSize; i++) {
            System.out.print(array[i]+", ");
        }
    }

    public void printFromLast(){
        int i=currentSize-1;
        do{
            System.out.print(array[i]+", ");
            i--;
        }while(i>=0);
    }


    public void remove (){
        array[currentSize-1]=null;
        currentSize--;
    }

    public void removeAtPosition(int index){
        for (int i = 0; i < currentSize; i++) {
            array[index+i]=array[index+i+1];
        }
        array[currentSize-1]=null;
        currentSize--;
    }

    public Integer size (){
        return currentSize;
    }


}
