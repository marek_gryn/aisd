package arrayList;

import java.util.Random;

/**
 * Created by Marek on 04.09.2017.
 */
public class MyMain {
    public static void main(String[] args) {


        MyArrayList list = new MyArrayList();

        list.add(5);
        list.add(8);
        list.add(9);
        list.add(2);
        list.add(3);
        list.add(7);

        list.printList();
        System.out.println();

        list.add(4,1);
        list.printList();

        list.remove();
        list.printList();
        System.out.println();
        System.out.println("Size: "+list.size());
        list.removeAtPosition(1);
        list.printList();
        System.out.println();
        System.out.println("Size: "+list.size());
        System.out.println();
        System.out.println();




        list.printFromLast();
        System.out.println();

        System.out.println("Size: "+list.size());

    }
}
