package oneDirectionalList;

import java.util.Random;

/**
 * Created by Marek on 04.09.2017.
 */
public class MyMain {
    public static void main(String[] args) {
        Random random=new Random();
        int temp;
        int root;



        MyList list = new MyList();

        list.add(5);
        list.add(8);
        list.add(9);
        list.add(2);
        list.add(3);
        list.add(7);

        list.printList();
        System.out.println();
        list.printListFromLast();

/*        list.remove();
        list.remove();
        list.remove();

        System.out.println();
        list.printList();

        System.out.println();
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));
        System.out.println(list.get(4));*/


        System.out.println();

        list.add(999,1);
        list.printList();
        System.out.println();
        list.add(99,1);
        list.printList();
        System.out.println();
        list.printListFromLast();
        System.out.println();
        System.out.println();
        list.printList();
        System.out.println();
        list.removeAtPosition(1);
        list.printList();

        System.out.println();
        System.out.println("Size: "+list.size());

    }
}
