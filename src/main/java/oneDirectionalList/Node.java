package oneDirectionalList;



/**
 * Created by Marek on 04.09.2017.
 */
public class Node {

    private Object data;
    private Node next;
    private Node prev;

    public Node( Object data) {

        this.data = data;

    }


    public Node getPrev() {
        return prev;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node nextId) {
        this.next = nextId;
    }


}
