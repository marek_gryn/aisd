package oneDirectionalList;

/**
 * Created by Marek on 04.09.2017.
 */
public class MyList {
    private Node root;
    private Node tail;

    public MyList() {
        this.root=null;
    }

    public void add(Object data) {
        Node wezel = new Node(data);

        if (root == null) {
            root = wezel;
            tail =root;
        } else {
            Node tmp = root;
            Node tmpPrev=null;
            while (tmp.getNext() != null) {
                tmpPrev=tmp;
                tmp = tmp.getNext();
            }
            tmp.setNext(wezel);
            tmp.setPrev(tmpPrev);
            tail = wezel;
            tail.setPrev(tmp);
        }
    }

    public void remove(){
        if(root==null){
            return;
        }
        if(root==tail){
            root=tail=null;
        }
        if(root!=null){
            root = root.getNext();
            root.setPrev(null);
        }
    }

    public void printList (){
        Node tmp=root;

        if(tmp==null){
            return;
        }

        while (tmp.getNext() != null){
            System.out.print(tmp.getData()+", ");
            tmp=tmp.getNext();
        }
        System.out.print(tmp.getData());
    }

    public void printListFromLast (){
        Node tmp= tail;

        if(tmp==null){
            return;
        }

        while (tmp.getPrev() != null){
            System.out.print(tmp.getData()+ ", ");
            tmp=tmp.getPrev();
        }
        System.out.print(tmp.getData());

    }

    public Object get(int index){
        if(root==null){
            return null;
        }
        else{
            int counter=0;
            Node tmp = root;
            while(tmp.getNext()!=null && counter != index){
                tmp=tmp.getNext();
                counter++;
            }

            if(counter==index){
                return tmp.getData();
            }else{
                System.out.println("NoSuchElementException");
                return null;
            }
        }
    }

   /* public void add(Object data, int index){
        Node wezel = new Node(data);


        if (root == null) {
            root = wezel;
        } else {
            Node tmp = root;
            int counter=0;
            Node tmpPrev;
            tmpPrev=tmp;
            tmp=tmp.getNext();
            if(tmpPrev.getNext()==null){
                wezel.setNext(tmpPrev.getNext());
                tmpPrev.setNext(wezel);
            }
            while (tmp.getNext() != null && counter<=index) {

                if(counter==index){
                    wezel.setNext(tmp.getNext());
                    tmp.setNext(wezel);
                    break;
                }
                tmp = tmp.getNext();

                counter++;
            }
        }
    }*/

    public void add(Object data, int index) {
        Node wezel = new Node(data);

        int licznik = 0;
        Node tmp = root;    // przechodzimy do n-tego elementu
        Node tmpPrev = null; // poprzedni element od tmp
        while (tmp.getNext() != null && licznik != index) { // dopóki istnieje następny element
            tmpPrev = tmp;  // poprzedni element to tmp, tmp to next
            tmp = tmp.getNext();        // przejdź do następnego elementu
            licznik++; // inkrementacja licznika
        }

        if (tmpPrev == null) { // wstawiamy zamiast root'a (czyli jako 1 element)
            wezel.setNext(root);
            root = wezel;
        } else if (licznik == index) { // dotarliśmy do n-tego elementu
            wezel.setNext(tmpPrev.getNext());
            tmpPrev.setNext(wezel);
            tmp.setPrev(wezel);
            wezel.setPrev(tmpPrev);
        } else if (tmp.getNext() == null && (licznik + 1) == index) { // wstawiamy na ost. indeks
            wezel.setNext(tmp.getNext());
            tmp.setNext(wezel);
            wezel.setPrev(tail);
            tail=wezel;
        } else {
            System.out.println("nie moge dodac elementu, arrayindexoutofbounds");
        }
    }

    public void removeAtPosition(int index){
        int counter=1;
        Node tmp=root;
        Node tmpPrev=tmp;
        tmp=tmp.getNext();
        if(index==0){
            root=tmp;
            tmpPrev.setNext(null);

        }else{

            while (tmp.getNext() != null && counter<=index){

                if(counter==index){

                    tmpPrev.setNext(tmp.getNext());
                    if(tmp.getNext()!=null){
                        tmp.getNext().setPrev(tmpPrev);
                    }
                    if(tmp==tail){
                        tail=tmpPrev;
                    }



                    tmp.setNext(null);
                    tmp.setPrev(null);
                    break;
                }
                tmpPrev=tmp;
                tmp = tmp.getNext();
                counter++;
            }
        }

    }

    public int size(){
        int counter=0;
        Node tmp=root;
        while (tmp.getNext() != null){
            tmp=tmp.getNext();
            counter++;
        }
        return counter+1;
    }
}
